load('ecgConditioningExample.mat');
time=1/fs:1/fs:30;

connectedSignals=[];

%% Check channel connection: 
%creates a new matrix only with connected channels (rms different from 0)

for i=1:size(ecg,2)
    
    if rms(ecg(:,i))>0
        connectedSignals=[connectedSignals ecg(:,i)];
    else
        continue
    end
end


%% Spikes removal: 
% hempel function removes outliers values using the median of a window 
%composed of the sample and 5 surrounding samples. If a sample differs from
%the median by more than 5 standard deviations, it is replaced with the median

for j=1:size(connectedSignals,2)
    connectedSignals(:,j)=hampel(connectedSignals(:,j),5,5);
end

%% Low-Pass (Cut-Off Freq:90Hz) and BandStop(Cut-Off Freq:50Hz) Filtering

filteredSignals=zeros(size(connectedSignals));

%Filter design
d1 = designfilt('lowpassiir','FilterOrder',8, ...
         'PassbandFrequency',90, ...
         'SampleRate',fs); 

           
d2 = designfilt('bandstopiir','FilterOrder',20, ...
               'HalfPowerFrequency1',40,'HalfPowerFrequency2',60, ...
               'SampleRate',fs);
           
 %Filter application          
for k=1:size(filteredSignals,2)
    filteredSignals(:,k)=filtfilt(d1,connectedSignals(:,k));
    filteredSignals(:,k)=filtfilt(d2,filteredSignals(:,k));
end
           
%% Baseline removal and Signal Visualization
%Using matlab function detrend()

finalSignals=zeros(size(filteredSignals));

for m=1:size(finalSignals,2)
    finalSignals(:,m)=detrend(filteredSignals(:,m));
    
    figure
    plot(time,finalSignals(:,m))
    title(['ECG SIGNAL ' num2str(m)'])
    xlabel('Time(s)')
    ylabel('Amplitude')
    ylim([-15000 20000])
   
end




